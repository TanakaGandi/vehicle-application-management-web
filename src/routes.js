import React from "react";

const Dashboard = React.lazy(() => import("./screens/dashboard/Dashboard.js"));

//Claims
const Claims = React.lazy(() => import("./screens/claims/ClaimsScreen.js"));
const CancelClaim = React.lazy(() => import("./screens/claims/CancelClaim.js"));
const DealersScreen = React.lazy(() => import("./screens/dealers/DealersScreen.js"));
const LodgeClaim = React.lazy(() => import("./screens/claims/LodgeClaim.js"));
const SubmitDocs = React.lazy(() => import("./screens/claims/SubmitDocumentation.js"));
const TrackClaim = React.lazy(() => import("./screens/claims/TrackClaim.js"));
const AdministerClaim = React.lazy(()=>import("./screens/claims/admin/AdministerClaim.js"));
const DamageScreen = React.lazy(()=>import("./screens/claims/DamageScreen.js"));
const QuotationsScreen =React.lazy(()=>import("./screens/quotations/QuotationsScreen.js"))
const CompanysScreen = React.lazy(()=>import("./screens/quotations/CompanysScreen.js"))


const routes = [
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  
  //Claims
  { path: "/claims", name: "Claims Management", component: Claims },
  { path: "/cancel", name: "Cancel Claim", component: CancelClaim },
  { path: "/lodge", name: "Lodge Claim", component: LodgeClaim },
  { path: "/submit", name: "Submit Documentation", component: SubmitDocs },
  { path: "/track", name: "Track Claim", component: TrackClaim },
  { path: "/adminster", name:"Adminster Claims", component: AdministerClaim},
  { path: "/damage", name:"Assess Damage", component: DamageScreen},
  { path: "/quotation", name:"Quotations", component: QuotationsScreen},
  { path:"/quotes", name:"Quotations", component:CompanysScreen},

  //Dealers
  { path: "/find", name: "Service Providers", component: DealersScreen },
  
];

export default routes;
