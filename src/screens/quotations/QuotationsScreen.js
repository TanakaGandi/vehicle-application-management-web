import React from "react";
import useTrans from "../../hooks/useTrans";
import ScreensCards from"../../components/cards/ScreensCards"
import { Row, Col } from "reactstrap";
import { Link } from "react-router-dom";

const QuotationsScreen = () => {
  // eslint-disable-next-line
  const [t, handleClick] = useTrans();
  return (
    <div className="animated fadeIn">
      <Link to="/dashboard" className="mb-2">
        <span class="material-icons">keyboard_backspace</span>
      </Link>
      <Row>
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("GetQuotation.1")}
            footerText={t("GetQuotation.1")}
            cardRoute="quotes"
          />
        </Col>
      </Row>
    </div>
  );
};

export default QuotationsScreen;
